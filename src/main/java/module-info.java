module Patient.register {
    requires javafx.controls;
    requires javafx.fxml;
    exports edu.ntnu.idatt2001.mappe2.mikkelof;
    exports edu.ntnu.idatt2001.mappe2.mikkelof.controllers;
}