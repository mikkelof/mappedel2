package edu.ntnu.idatt2001.mappe2.mikkelof;

/**
 * A singleton-class for holding patients so that other classes can use them
 * @author Mikkel Ofrim
 * @version 1.0
 */

public final class PatientHolder {
    private Patient patient;
    private final static PatientHolder INSTANCE = new PatientHolder();

    /**
     * The constructor in a singleton class is private
     */
    private PatientHolder() {}

    public static PatientHolder getInstance() {
        return INSTANCE;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Patient getPatient() {
        return this.patient;
    }
}
