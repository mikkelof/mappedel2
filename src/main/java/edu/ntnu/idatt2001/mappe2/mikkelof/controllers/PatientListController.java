package edu.ntnu.idatt2001.mappe2.mikkelof.controllers;
import edu.ntnu.idatt2001.mappe2.mikkelof.FileHandler;
import edu.ntnu.idatt2001.mappe2.mikkelof.Patient;
import edu.ntnu.idatt2001.mappe2.mikkelof.PatientHolder;
import edu.ntnu.idatt2001.mappe2.mikkelof.PatientRegister;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

/**
 * Controller-class for the patient-list
 * @author Mikkel Ofrim
 * @version 1.0
 */

public class PatientListController {

    @FXML
    public TableView<Patient> patientTableView;
    public TableColumn<Patient, String> firstNameColumn, lastNameColumn, socialSecurityNumberColumn, diagnosisColumn, generalPractitionerColumn;

    private static ObservableList<Patient> patientList = FXCollections.observableArrayList();

    /**
     * Method for updating the patient register
     */

    public static void updatePat() {
        patientList.clear();
        patientList.addAll(PatientRegister.getPatients());
    }

    /**
     * Method for selecting a patient and saving it in the PatientHolder
     * @throws Exception
     */

    public void setSelectedPatient() throws Exception {
        PatientHolder patientHolder = PatientHolder.getInstance();
        if (patientTableView.getSelectionModel().getSelectedItem() == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("No patient selected");
            alert.show();
            throw new Exception("No patient selected");
        }

        patientHolder.setPatient(patientTableView.getSelectionModel().getSelectedItem());
    }

    /**
     * Method for saving the csv-file containing all the data from the register
     * @throws IOException
     */

    public void exportToCsv() throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV files (*csv)", ".csv"));
        File saveFile = fileChooser.showSaveDialog(patientTableView.getScene().getWindow());

        if (!saveFile.createNewFile()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("File already exists");
            alert.setHeaderText("This file already exists.");
            alert.setContentText("Do you want to overwrite it?");

            Optional<ButtonType> resultBoolean = alert.showAndWait();
            if (resultBoolean.isPresent() && resultBoolean.get() != ButtonType.OK) {
                return;
            }
        }


        FileHandler.writeToCsv(saveFile.getPath());
    }

    /**
     * Method for importing all the data from a csv-file to the register. Lets the user select the location to save the file
     * and makes sure the file the user tries importing is a csv-file.
     * @throws Exception
     */

    public void importFromCsv() throws Exception {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Please select the csv file you wish to import");

        File file = fileChooser.showOpenDialog(patientTableView.getScene().getWindow());
        if (file==null) {
            return;
        }

        String fileName = file.getName();

        String extension = "";
        int i = fileName.lastIndexOf(".");
        if (i > 0) {
            extension = fileName.substring(i+1);
        }

        if(!extension.equals("csv")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Wrong filetype");
            alert.setHeaderText("The selected file-type is not supported");
            alert.setContentText("Please try again with a .csv file");
            alert.showAndWait();
            return;
        }

        FileHandler.readFromCsv(file.getPath());
    }

    public void initialize() throws Exception {
        patientList.clear();
        patientList.addAll(PatientRegister.getPatients());

        firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        diagnosisColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        patientTableView.setItems(patientList);
        patientTableView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    }

    public void exitApplication(ActionEvent event) {
        Platform.exit();
        System.exit(0);
    }

    public void openNewPatientWindow(ActionEvent event) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/AddNewPatient.fxml")));
        Stage stage = new Stage();
        stage.initStyle(StageStyle.DECORATED);
        stage.setTitle("Add new patient");
        stage.setScene(new Scene(root));
        stage.show();
    }

    public void openEditPatientWindow(ActionEvent event) throws Exception {
        setSelectedPatient();
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/EditPatient.fxml")));
        Stage stage = new Stage();
        stage.initStyle(StageStyle.DECORATED);
        stage.setTitle("Edit patient");
        stage.setScene(new Scene(root));
        stage.show();
    }

    public void deletePatient(ActionEvent event) throws Exception {
        setSelectedPatient();
        Patient patient = PatientHolder.getInstance().getPatient();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "", ButtonType.YES, ButtonType.NO);
        alert.setHeaderText("Remove " + patient.getFirstName() + " " + patient.getLastName() + " from the register?");
        alert.setResizable(false);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            PatientRegister.deletePatient(patient);
            patientList.clear();
            patientList.addAll(PatientRegister.getPatients());
        }
    }

    public void showInformation() {
        Alert infoAlert = new Alert(Alert.AlertType.INFORMATION);
        infoAlert.setTitle("About");
        infoAlert.setHeaderText("Patient register\nv0.1-SNAPSHOT");
        infoAlert.setContentText("Probably the best patient register ever\n(C)Mikkel Ofrim\n2021-04-25");
        infoAlert.show();
    }
}
