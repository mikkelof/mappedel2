package edu.ntnu.idatt2001.mappe2.mikkelof.controllers;

import edu.ntnu.idatt2001.mappe2.mikkelof.Patient;
import edu.ntnu.idatt2001.mappe2.mikkelof.PatientHolder;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller-class for editing patients
 * @author Mikkel Ofrim
 * @version 1.0
 */

public class EditPatientController {

    public EditPatientController() throws Exception {
    }

    @FXML
    public Button addPatientCancelButton, addPatientOKButton;
    public TextField editFirstNameTextField, editLastNameTextField, editSocSecNumTextField, editDiagnosisTextField, editGenPracTextField;

        Patient selectedPat = PatientHolder.getInstance().getPatient();

        public void initialize () {
            editSocSecNumTextField.setText(selectedPat.getSocialSecurityNumber());
            editFirstNameTextField.setText(selectedPat.getFirstName());
            editLastNameTextField.setText(selectedPat.getLastName());
            editDiagnosisTextField.setText(selectedPat.getDiagnosis());
            editGenPracTextField.setText(selectedPat.getGeneralPractitioner());
        }

        /**
        * Method for saving the data the user put in to the register of the OK button is clicked
        * @param event
        * @throws Exception
        */

        public void okEditPatient (ActionEvent event) throws Exception {
            Patient selectedPat = PatientHolder.getInstance().getPatient();
            selectedPat.setSocialSecurityNumber(editSocSecNumTextField.getText());
            selectedPat.setFirstName(editFirstNameTextField.getText());
            selectedPat.setLastName(editLastNameTextField.getText());
            selectedPat.setDiagnosis(editDiagnosisTextField.getText());
            selectedPat.setGeneralPractitioner(editGenPracTextField.getText());
            PatientListController.updatePat();
            Stage stage = (Stage) addPatientOKButton.getScene().getWindow();
            stage.close();
        }

    /**
     * Method for cancelling if the cancel button is clicked
     * @param event
     */

    public void cancelEditPatient (ActionEvent event){
            Stage stage = (Stage) addPatientCancelButton.getScene().getWindow();
            stage.close();
        }
    }
