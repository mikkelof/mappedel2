package edu.ntnu.idatt2001.mappe2.mikkelof;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

/**
 * Factory-class
 * @author Mikkel Ofrim
 * @version 1.0
 */

public class Factory {
    /**
     * A factory method for creating objects that inherits from node
     * @param nodeType The type of object the user wants
     * @param x The value of the property layoutX
     * @param y The value of the property layoutY
     * @return returns the object the user requested with the properties the user set
     */

    public static Node getNode(String nodeType, double x, double y) {
        if (nodeType.equalsIgnoreCase("BUTTON")) {
            Button button = new Button();
            button.setLayoutX(x);
            button.setLayoutY(y);
            return button;
        }
        else if (nodeType.equalsIgnoreCase("TABLEVIEW")) {
            TableView tableView = new TableView<>();
            tableView.setLayoutX(x);
            tableView.setLayoutY(y);
            return tableView;
        }
        else if (nodeType.equalsIgnoreCase("TEXTFIELD")) {
            TextField textField = new TextField();
            textField.setLayoutX(x);
            textField.setLayoutY(y);
            return textField;
        }
        else if (nodeType.equalsIgnoreCase("MENUBAR")) {
            MenuBar menuBar = new MenuBar();
            menuBar.setLayoutX(x);
            menuBar.setLayoutY(y);
            return menuBar;
        }
        else if (nodeType.equalsIgnoreCase("ANCHORPANE")) {
            AnchorPane anchorPane = new AnchorPane();
            anchorPane.setLayoutX(x);
            anchorPane.setLayoutY(y);
            return anchorPane;
        }
        else if (nodeType.equalsIgnoreCase("TEXT")) {
            Text text = new Text();
            text.setLayoutX(x);
            text.setLayoutY(y);
            return text;
        }
        else if (nodeType.equalsIgnoreCase("IMAGEVIEW")) {
            ImageView imageView = new ImageView();
            imageView.setLayoutX(x);
            imageView.setLayoutY(y);
            return imageView;
        }
        else {
            return null;
        }
    }
}
