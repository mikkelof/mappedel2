package edu.ntnu.idatt2001.mappe2.mikkelof;

import java.io.*;

/**
 * A class file-handling
 * @author Mikkel Ofrim
 * @version 1.0
 */

public class FileHandler {

    /**
     * A method for writing the data from the register and save it as a csv file
     * @param filePath The path to where the user wants to save the csv file
     * @throws IOException
     */

    public static void writeToCsv(String filePath) throws IOException {
        File file = new File(filePath);
        FileWriter writer = new FileWriter(file);
        for (Patient patient : PatientRegister.getPatients()) {
            writer.write(patient.getFirstName() + ";" + patient.getLastName() + ";" + patient.getGeneralPractitioner() + ";" + patient.getSocialSecurityNumber() + ";" + patient.getDiagnosis() + "\n");
        }
        writer.close();
    }

    /**
     * A method for reading the data from a csv-file and put it in the register
     * @param filePath the path to the csv file the user wants to import
     * @throws Exception
     */

    public static void readFromCsv(String filePath) throws Exception {
        String row;
        PatientRegister.getPatients().clear();
        BufferedReader csvReader = new BufferedReader(new FileReader(filePath));
        while((row = csvReader.readLine()) != null) {
            String[] data = row.split(";");
            Patient patient = new Patient(data[3], data[0], data[1],data[4], data[2]);
            PatientRegister.addPatient(patient);
        }
        csvReader.close();
    }
}
