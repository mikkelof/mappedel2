package edu.ntnu.idatt2001.mappe2.mikkelof.controllers;

import edu.ntnu.idatt2001.mappe2.mikkelof.Patient;
import edu.ntnu.idatt2001.mappe2.mikkelof.PatientRegister;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller-class for adding new patients to the register
 * @author Mikkel Ofrim
 * @version 1.0
 */

public class AddNewPatientController {

    @FXML
    public Button addPatientCancelButton, addPatientOKButton;
    public TextField firstNameTextField, lastNameTextField, socSecNumTextField, diagnosisTextField, genPracTextField;

    /**
     * Method for saving the data the user put in to the register of the OK button is clicked
     * @param event
     * @throws Exception
     */

    public void okAddNewPatient(ActionEvent event) throws Exception {
        Patient newPat = new Patient(socSecNumTextField.getText(), firstNameTextField.getText(), lastNameTextField.getText(), diagnosisTextField.getText(), genPracTextField.getText());
        PatientRegister.addPatient(newPat);
        Stage stage = (Stage) addPatientOKButton.getScene().getWindow();
        stage.close();
    }

    /**
     * Method for cancelling if the cancel button is clicked
     * @param event
     * @throws Exception
     */

    public void cancelAddNewPatient(ActionEvent event) {
        Stage stage = (Stage) addPatientCancelButton.getScene().getWindow();
        stage.close();
    }
}
