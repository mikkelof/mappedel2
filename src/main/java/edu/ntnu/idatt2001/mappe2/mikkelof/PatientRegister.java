package edu.ntnu.idatt2001.mappe2.mikkelof;

import edu.ntnu.idatt2001.mappe2.mikkelof.controllers.PatientListController;

import java.util.ArrayList;

/**
 * Class for the PatientRegister.
 * @author Mikkel Ofrim
 * @version 1.0
 */

public class PatientRegister {
    private static ArrayList<Patient> patients;

    private PatientRegister() {
        throw new IllegalStateException("This is a utility class");
    }

    /**
     * A method for adding patients to the register that checks if a patient with the same social security number is already registered
     * @param patient The patient the user wants to add to the register
     * @throws Exception
     */

    public static void addPatient(Patient patient) throws Exception {
        if(getPatients().stream().noneMatch(Pat -> patient.getSocialSecurityNumber().equals(Pat.getSocialSecurityNumber()))) {
            patients.add(patient);
            PatientListController.updatePat();
        }
        else {
            throw new Exception("Patient is already registered");
        }
    }

    public static ArrayList<Patient> getPatients() {
        if (patients == null) {
            loadPatients();
        }
        return patients;
    }

    public static void loadPatients() {
        patients = new ArrayList<>();
    }

    /**
     * A method for removing a patient if it exists in the register
     * @param patient The patient the user wants to remove from the register
     * @throws Exception
     */

    public static void deletePatient(Patient patient) throws Exception {
        if(!getPatients().contains(patient)) {
            throw new Exception("Patient not in the register");
        }
        else {
            getPatients().remove(patient);
        }
    }
}
