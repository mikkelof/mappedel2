import edu.ntnu.idatt2001.mappe2.mikkelof.Patient;
import edu.ntnu.idatt2001.mappe2.mikkelof.PatientRegister;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

public class RegisterTest {
    @Test
    @DisplayName("Test if the patient-list grows by one if a patient is added to the register")
    public void checkIfListGrowsByOneWhenPatientIsAddedTest() throws Exception {
        PatientRegister.getPatients().clear();
        Patient patient = new Patient("17233872", "Knut", "Knutson", "Broken foot", "Jon Olav");
        PatientRegister.addPatient(patient);
        Assertions.assertEquals(1, PatientRegister.getPatients().size());
    }

    @Test
    @DisplayName("Test if the patient-list shrinks by one if a patient is removed from the register")
    public void checkIfListShrinksByOneWhenPatientIsRemovedTest() throws Exception {
        PatientRegister.getPatients().clear();
        Patient patient = new Patient("17233872", "Knut", "Knutson", "Broken foot", "Jon Olav");
        PatientRegister.addPatient(patient);
        int startSize = PatientRegister.getPatients().size();
        PatientRegister.deletePatient(patient);
        Assertions.assertEquals(startSize - 1, PatientRegister.getPatients().size());
    }

    @Test
    @DisplayName("Test if an Exception is thrown if you add a patient that is already in the list")
    public void checkIfExceptionIsThrownWhenAddingPatientAlreadyInListTest() throws Exception {
        PatientRegister.getPatients().clear();
        Patient patient = new Patient("17233872", "Knut", "Knutson", "Broken foot", "Jon Olav");
        PatientRegister.addPatient(patient);
        Assertions.assertThrows(Exception.class, () -> PatientRegister.addPatient(patient));
    }

    @Test
    @DisplayName("Test if an Exception is thrown if you remove a patient that is not in the list")
    public void checkIfExceptionIsThrownWhenRemovingPatientNotInListTest() {
        PatientRegister.getPatients().clear();
        Patient patient = new Patient("17233872", "Knut", "Knutson", "Broken foot", "Jon Olav");
        Assertions.assertThrows(Exception.class, () -> PatientRegister.deletePatient(patient));
    }
}
