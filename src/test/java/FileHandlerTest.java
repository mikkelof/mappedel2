import edu.ntnu.idatt2001.mappe2.mikkelof.FileHandler;
import edu.ntnu.idatt2001.mappe2.mikkelof.Patient;
import edu.ntnu.idatt2001.mappe2.mikkelof.PatientRegister;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.nio.file.Files;
import java.nio.file.Path;

public class FileHandlerTest {
    @Test
    public void checkIfReadingFromFileWorks() throws Exception {
        FileHandler.readFromCsv("src/testdata/TestFile.csv");
        Assertions.assertEquals("123456789", PatientRegister.getPatients().get(0).getSocialSecurityNumber());
    }

    @Test
    public void checkIfWritingToFileWorks() throws Exception {
        FileHandler.readFromCsv("src/testdata/TestFile.csv");
        FileHandler.writeToCsv("src/testdata/NewTestFile.csv");
        Assertions.assertTrue(Files.deleteIfExists((Path.of("src/testdata/NewTestFile.csv"))));
    }
}
